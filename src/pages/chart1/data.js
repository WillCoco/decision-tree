'use strict';

import BaseNode from './node';
import BaseEdge from './edge';

export default {
  nodes: [{
    id: '0',
    text: '学历 0',
    top: 10,
    left: 510,
    width: 100,
    height: 60,
    color: 'orange',
    shape: 'circle',
    border: 'dashed',
    Class: BaseNode,
    endpoints: [{
      id: 'top',
      orientation: [0, -1],
      pos: [0.5, 0]
    }, {
      id: 'right',
      orientation: [1, 0],
      pos: [0, 0.5]
    }, {
      id: 'bottom',
      orientation: [0, 1],
      pos: [0.5, 0]
    }, {
      id: 'left',
      orientation: [-1, 0],
      pos: [0, 0.5]
    }]
  }, {
    id: '1',
    text: '本科及以上 1',
    top: 137,
    left: 500,
    width: 100,
    height: 60,
    color: 'purple',
    shape: 'diamond',
    border: 'solid',
    Class: BaseNode,
  }, {
    id: '2',
    text: '年薪 2',
    top: 150,
    left: 250,
    width: 100,
    height: 60,
    color: 'green',
    shape: 'rect',
    border: 'solid',
    Class: BaseNode,
    endpoints: [{
      id: 'top',
      orientation: [0, -1],
      pos: [0.5, 0]
    }, {
      id: 'right',
      orientation: [1, 0],
      pos: [0, 0.5]
    }, {
      id: 'bottom',
      orientation: [0, 1],
      pos: [0.5, 0]
    }, {
      id: 'left',
      orientation: [-1, 0],
      pos: [0, 0.5]
    }]
  }, {
    id: '4',
    text: '年龄 4',
    top: 150,
    left: 750,
    width: 100,
    height: 60,
    color: 'orange',
    shape: 'rect',
    border: 'dashed',
    Class: BaseNode,
    endpoints: [{
      id: 'top',
      orientation: [0, -1],
      pos: [0.5, 0]
    }, {
      id: 'right',
      orientation: [1, 0],
      pos: [0, 0.5]
    }, {
      id: 'bottom',
      orientation: [0, 1],
      pos: [0.5, 0]
    }, {
      id: 'left',
      orientation: [-1, 0],
      pos: [0, 0.5]
    }]
  }, {
    id: '5',
    text: '30岁及以上 5',
    top: 280,
    left: 250,
    width: 100,
    height: 60,
    color: 'green',
    shape: 'diamond',
    border: 'dashed',
    Class: BaseNode,
  },{
    id: '6',
    text: '30W及以上 6',
    top: 279,
    left: 751,
    width: 100,
    height: 60,
    color: 'green',
    shape: 'diamond',
    border: 'dashed',
    Class: BaseNode,
  }, {
    id: '7',
    text: '同意 7',
    top: 447,
    left: 249,
    width: 100,
    height: 60,
    color: 'green',
    shape: 'rect',
    border: 'dashed',
    Class: BaseNode,
    endpoints: [{
      id: 'top',
      orientation: [0, -1],
      pos: [0.5, 0]
    }, {
      id: 'right',
      orientation: [1, 0],
      pos: [0, 0.5]
    }, {
      id: 'bottom',
      orientation: [0, 1],
      pos: [0.5, 0]
    }, {
      id: 'left',
      orientation: [-1, 0],
      pos: [0, 0.5]
    }]
  }, {
    id: '8',
    text: '拒绝 8',
    top: 452,
    left: 751,
    width: 100,
    height: 60,
    color: 'green',
    shape: 'rect',
    border: 'dashed',
    Class: BaseNode,
    endpoints: [{
      id: 'top',
      orientation: [0, -1],
      pos: [0.5, 0]
    }, {
      id: 'right',
      orientation: [1, 0],
      pos: [0, 0.5]
    }, {
      id: 'bottom',
      orientation: [0, 1],
      pos: [0.5, 0]
    }, {
      id: 'left',
      orientation: [-1, 0],
      pos: [0, 0.5]
    }]
  },  {
    id: '9',
    text: '同意 9',
    top: 295,
    left: 71,
    width: 100,
    height: 60,
    color: 'green',
    shape: 'rect',
    border: 'dashed',
    Class: BaseNode,
    endpoints: [{
      id: 'top',
      orientation: [0, -1],
      pos: [0.5, 0]
    }, {
      id: 'right',
      orientation: [1, 0],
      pos: [0, 0.5]
    }, {
      id: 'bottom',
      orientation: [0, 1],
      pos: [0.5, 0]
    }, {
      id: 'left',
      orientation: [-1, 0],
      pos: [0, 0.5]
    }]
  }, {
    id: '10',
    text: '拒绝 10',
    top: 299,
    left: 923,
    width: 100,
    height: 60,
    color: 'green',
    shape: 'rect',
    border: 'dashed',
    Class: BaseNode,
    endpoints: [{
      id: 'top',
      orientation: [0, -1],
      pos: [0.5, 0]
    }, {
      id: 'right',
      orientation: [1, 0],
      pos: [0, 0.5]
    }, {
      id: 'bottom',
      orientation: [0, 1],
      pos: [0.5, 0]
    }, {
      id: 'left',
      orientation: [-1, 0],
      pos: [0, 0.5]
    }]
  }, ],
  edges: [{
    source: 'bottom',
    target: 'top',
    sourceNode: '0',
    targetNode: '1',
    type: 'endpoint',
    color: 'black',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  }, {
    source: 'right',
    target: 'left',
    sourceNode: '1',
    targetNode: '4',
    type: 'endpoint',
    color: 'black',
    label: 'yes',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  }, {
    source: 'left',
    target: 'right',
    sourceNode: '1',
    targetNode: '2',
    type: 'endpoint',
    color: 'purple',
    label: 'no',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  },{
    source: 'left',
    target: 'right',
    sourceNode: '2',
    targetNode: '3',
    color: 'black',
    type: 'endpoint',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  }, {
    source: 'bottom',
    target: 'top',
    sourceNode: '2',
    targetNode: '5',
    color: 'green',
    type: 'endpoint',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  }, {
    source: 'bottom',
    target: 'top',
    sourceNode: '4',
    targetNode: '6',
    color: 'green',
    type: 'endpoint',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  }, {
    source: 'left',
    target: 'right',
    sourceNode: '5',
    targetNode: '9',
    color: 'green',
    type: 'endpoint',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  }, {
    source: 'bottom',
    target: 'top',
    sourceNode: '5',
    targetNode: '7',
    color: 'green',
    type: 'endpoint',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  }, {
    source: 'bottom',
    target: 'top',
    sourceNode: '6',
    targetNode: '8',
    color: 'green',
    type: 'endpoint',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  }, {
    source: 'right',
    target: 'left',
    sourceNode: '6',
    targetNode: '10',
    color: 'green',
    type: 'endpoint',
    arrow: true,
    arrowPosition: 1,
    Class: BaseEdge
  }],
  group: []
};


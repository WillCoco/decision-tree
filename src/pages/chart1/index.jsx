import {Canvas} from 'butterfly-dag';
import React, {useEffect, useRef} from 'react';

import mockData from './data.js';

import './index.less';
import 'butterfly-dag/dist/index.css';

export default () => {
  const canvasRef = useRef()

  useEffect(() => {
    let root = document.getElementById('dag-canvas');
    canvasRef.current = new Canvas({
      root: root,
      disLinkable: true, // 可删除连线
      linkable: true,    // 可连线
      draggable: true,   // 可拖动
      zoomable: true,    // 可放大
      moveable: true,    // 可平移
      theme: {
        edge: {
          shapeType: 'Manhattan',
          arrow: true
        },
        // 拖动边缘处自动适应画布
        autoFixCanvas: {
          enable: true
        }
      },
    });

    canvasRef.current.on('system.drag.end', (data) => {
      console.log(data, 'data')
      //data 数据
    });

    canvasRef.current.draw(mockData);
  }, [])

  return (
    <div className='emergency-page'>
      <div className="emergency-canvas" id="dag-canvas">
      </div>
    </div>
  );
}


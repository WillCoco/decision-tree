import React from 'react';
import { useState } from 'react';
import Chart1 from './chart1';
import Chart2 from './chart2';

function Pages() {
  const [showChart1, setShowChart1] = useState(true);
  return (
    <div style={{height: '100vh'}}>
      <a onClick={() => setShowChart1(v => !v)}>切换</a>
      {
        showChart1 ? (
          <>
            {`  图表1`}
            <Chart1 />
          </>
        ) : (
          <>
            {`  图表2`}
            <Chart2 />
          </>
        )
      }
    </div>
  );
}

export default Pages;
